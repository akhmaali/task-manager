import React, { Component } from 'react';
import Board from './Board';
import ActionButton from './ActionButton';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import './styles/App.css';
import UIfx from 'uifx';
import flipAudio from './sounds/page-flip.mp3';
import weedAudio from './sounds/ＳＭＯＫＥ  ＣＨＩＬＬ.mp3';

/**
 * Card drag sound, which is played when card is dragged 
 */
const pageFlipSound = new UIfx(
  flipAudio,
  {
    volume: 0.4,
    throttleMs: 100
  }
)


/**
 * Theme music, which can be turned on by clicking the svg music icon in the header
 */
const smokeChillSound = new UIfx(
  weedAudio,
  {
    volume: 0.9,
    throttleMs: 100
  }
)

/**
 * @component App - represents the main component in app
 */
class App extends Component {

  /**
   * Constructor for App component
   * State contains the boards, that are shown by default when first app is run
   * State also contains some properties used for different actions(controlling input fields, etc.)
   * @param {*} props 
   */
  constructor(props) {
    super(props);
    this.state = {
      boards: [
        {
            title: "Open",
            id: `board-0`,
            cards: []
        }, 
        {
          title: "To Do",
          id: `board-1`,
          cards:[]
        },
        {
          title: "In Progress",
          id: `board-2`,
          cards:[]
        },
        {
          title: "Done",
          id: `board-3`,
          cards: []
        }
      ],
      showingCard: {},
      showPopup: false,
      inputTitle: '',
      inputDate: '',
      inputDescription: '',
      priorityShowing: '',
      deadlineShowing: '',
      cardIdToTransform: '',
      modifyingDeadline: false,
      weedPlaying: false
    }

    this.modal = React.createRef();
    this.popupContent = React.createRef();
    this.popupMessage = React.createRef();
    this.selectPriority = React.createRef();
    this.deadline = React.createRef();

    this.updateState = this.updateState.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    this.showModalPopup = this.showModalPopup.bind(this);
    this.hidePopup = this.hidePopup.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.triggerDragStart = this.triggerDragStart.bind(this);
    this.handleModifyButton = this.handleModifyButton.bind(this);
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleDeadlineChange = this.handleDeadlineChange.bind(this);
  }


  /**
   * When card is dropped, it is placed to the corresponding board at the required position
   * On drag end event, event contains destination, source, type properties,
   * which are used to navigate the card to correct position
   */
  onDragEnd = (result) => {
    this.setState({
      cardIdToTransform: ''
    })

    const { destination, source, type } = result;

    if(!destination) {
      return;
    }
    
    let droppableIdStart = source.droppableId;
    let droppableIdEnd = destination.droppableId;
    let droppableIndexStart = source.index;
    let droppableIndexEnd = destination.index;
    const newState = this.state.boards;

    //draggind boards around
    if(type === "board") {
        const board = newState.splice(droppableIndexStart, 1);
        newState.splice(droppableIndexEnd, 0, ...board);
        this.setState({
          boards: newState
        });
        localStorage.setItem('main',JSON.stringify(newState));
        return newState;
    }

    
    //drag happens in the same container(board)
    if(droppableIdStart === droppableIdEnd) {
        const board = this.state.boards.find(board => board.id === droppableIdStart);
        const card = board.cards.splice(droppableIndexStart, 1);
        board.cards.splice(droppableIndexEnd, 0, ...card);
        this.setState({
          boards: newState
        });
        localStorage.setItem('main',JSON.stringify(newState));
      }

    //card is moved to another board
    if(droppableIdStart !== droppableIdEnd) {

        //find the board where the drag started
        const boardStart = this.state.boards.find(board => board.id === droppableIdStart);

        //pull out the card from this board
        const card = boardStart.cards.splice(droppableIndexStart,1);

        //find the board where drag ended
        const boardEnd = this.state.boards.find(board => board.id === droppableIdEnd);

        //put the card into the new board
        boardEnd.cards.splice(droppableIndexEnd, 0, ...card);

        //set the state with the changes happened
        this.setState({
          boards: newState
        });

        //configure localStorage with changes
        localStorage.setItem('main',JSON.stringify(newState));
      }
  }

  /**
   * Used for updating App component from external components(Component drilling)
   */
  updateState = () => {
    let newBoards = JSON.parse(localStorage.getItem('main'));
    this.setState({
      boards: newBoards
    });
  }

  
  /**
   * Showing modal popup window for the specified card 
   * Is called from Board component, when clicked on the card
   */
  showModalPopup = (card) => {
    let newCard = {
      id: card.id,
      date: card.date,
      title: card.title,
      description: card.description,
      priority: card.priority,
      deadline: ''
    }

    this.setState({
      showingCard: newCard,
      showPopup: true,
      inputTitle: card.title,
      inputDescription: card.description,
      priorityShowing: card.priority,
      deadlineShowing: card.deadline,
      modifyingDeadline: false
    });
  }

  /**
   * Hides the popup window
   */
  hidePopup = () => {
    this.setState({
      showPopup: false
    });
  }

  /**
   * Controls title input field change
   */
  handleTitleChange = (e) => {
    this.setState({
      inputTitle: e.target.value
    });
  }

  /**
   * Controls description input change
   */
  handleDescriptionChange = (e) => {
    this.setState({
      inputDescription: e.target.value
    });
  }

  /**
   * Controls select 
   */
  handleSelectChange = (e) => {
    this.setState({
      priorityShowing: e.target.value
    })
  }

  /**
   * Controls deadline input field
   */
  handleDeadlineChange = (e) => {
    this.setState({
      deadlineShowing: e.target.value,
      modifyingDeadline: true
    });
  }


  /**
   * Triggers card/board drag start event
   * The sound effect for card dragging is played
   */
  triggerDragStart = (e) => {
    const { draggableId } = e;
    if(draggableId.startsWith('card')) {
      pageFlipSound.play();
    }
    this.setState({
      cardIdToTransform: draggableId
    });
  }

  /**
   * Triggers the "Modify the task" button(div-container) click
   * Get the boards from the localStorage
   * Find the board, in which card is being showed and modified in modal popup window
   * Find the card, which is being modified  
   * Apply changse given by the user to the retrieved card 
   * Persist changes back to localStorage and update the component
   */
  handleModifyButton = (e) => {
    let boards = JSON.parse(localStorage.getItem('main'));
    let searchedBoard = boards.find(board => board.cards.find(card => card.id === this.state.showingCard.id));
    let card = searchedBoard.cards.find(card => card.id === this.state.showingCard.id);
    card.title = this.state.inputTitle;
    card.description = this.state.inputDescription;
    card.priority = this.selectPriority.current.value;
    card.deadline = this.splitDate(this.deadline.current.value);
    localStorage.setItem('main', JSON.stringify(boards));
    this.setState({
      showPopup: false
    });
    this.updateState();
  }

  /**
   * Triggers theme music svg button clicked
   * Plays the corresponding music if it was not already started
   */
  triggerWeedMusic = (e) => {
    if(!this.state.weedPlaying) {
      smokeChillSound.play();
      this.setState({
        weedPlaying: true
      })
    }
  }

  /**
   * Splits date to desired format
   */
  splitDate = (date) => {
    let decomposed = date.split('-');
    return`${decomposed[2]}.${decomposed[1]}.${decomposed[0]}`;
  }

  /**
   * If localStorage is empty, persist boards from state to LS
   * (This is done for you,so that you see at least something when first opened the page:))
   * Else retrieve the boards from ls and set the state to it
   */
  componentDidMount() {
    if(localStorage.getItem('main')) {
      this.setState({
        boards: JSON.parse(localStorage.getItem('main'))
      })
    } else {
      localStorage.setItem('main', JSON.stringify(this.state.boards));
    }

    const canvas = this.refs.canvas;
    const ctx = canvas.getContext('2d');
    ctx.font = "20px Courier";
    ctx.fillStyle = "#4176a5";
    ctx.fillText('Editing Task', 10, 30);
  }

  
  /**
   * Rendering the component. Renders all boards from state(LS)
   * react-beautiful-dnd used for drag and drop implementation
   * Because using html5 Audio tag is a pain in React -> another approach (UIFX module)
   * DragDropContext
   * Modal popup window
   */
  render() {
    return (
      <div className = "main-container">
        <DragDropContext onDragEnd = {this.onDragEnd} onDragStart = {this.triggerDragStart}>
            <header className = "header">
              <h1 className = "app-title">Hello to my task manager</h1>
              <svg id = "svg-weed" className="svg-icon" viewBox="0 0 20 20" onClick = {this.triggerWeedMusic}>
                <path d="M16.899,3.05c-0.085-0.068-0.192-0.095-0.299-0.074L7.947,4.779c-0.17,0.034-0.291,0.182-0.291,0.353v7.364c-0.494-0.536-1.199-0.873-1.983-0.873c-1.491,0-2.704,1.213-2.704,2.704s1.213,2.704,2.704,2.704c1.491,0,2.705-1.213,2.705-2.704V7.952l7.933-1.659v4.399c-0.494-0.535-1.199-0.873-1.983-0.873c-1.491,0-2.704,1.213-2.704,2.704c0,1.492,1.213,2.705,2.704,2.705c1.49,0,2.704-1.213,2.704-2.705V3.33C17.031,3.221,16.982,3.119,16.899,3.05 M5.673,16.311c-1.094,0-1.983-0.889-1.983-1.983s0.889-1.983,1.983-1.983c1.095,0,1.983,0.889,1.983,1.983S6.768,16.311,5.673,16.311 M14.327,14.508c-1.095,0-1.983-0.889-1.983-1.984c0-1.094,0.889-1.982,1.983-1.982c1.094,0,1.983,0.889,1.983,1.982C16.311,13.619,15.421,14.508,14.327,14.508 M16.311,5.558L8.377,7.217V5.428l7.933-1.659V5.558z"></path>
              </svg>
            </header>

            <main>
              <Droppable droppableId = "all-lists" direction = "horizontal" type = "board">
                {provided => (
                  <div className = "board-container-app" {...provided.droppableProps} ref = {provided.innerRef}>
                      { this.state.boards.map((board, index) => (
                        <Board 
                          key = {board.id} 
                          boardID = {board.id} 
                          title = {board.title} 
                          cards = {board.cards} 
                          index = {index}
                          updateAppState = {this.updateState}
                          showModal = {this.showModalPopup}
                          cardIdToTransform = {this.state.cardIdToTransform}
                        />
                      ))}
                      {provided.placeholder}
                      <div className = "add-board-button">
                        <ActionButton updateAppState = {this.updateState} board />
                      </div>
                  </div>
                )}
              </Droppable>
            </main>
        </DragDropContext>

        <div className = "modal" style = {{display: this.state.showPopup ? 'flex' : 'none'}}>
          <div className = "modal-content">
            <form>
              <canvas className = "canvas" ref = "canvas" width = "170" height = "60"/>
              <div className = "close-container" onClick = {this.hidePopup}>+</div>


              <div className = "date-created-container">
                <p>{this.state.showingCard.date}</p>  
                <label>Date created</label>
              </div>

              <div className = "title-container">
                <label className = "title-label">Title</label>
                <input className = "title-input" type = "text" value = {this.state.inputTitle} onChange = {this.handleTitleChange} autoFocus></input>
              </div>

              <div className = "description-container">
                <label className = "description-label">Description</label>
                <textarea className = "description-textarea" type = "text" placeholder = "Task description ..." value = {this.state.inputDescription} onChange = {this.handleDescriptionChange}></textarea>
              </div>

              <div className = "priority-container">
                <label className = "priority-label">Priority</label>
                <select ref = {this.selectPriority} value = {this.state.priorityShowing} onChange = {this.handleSelectChange}>
                  <option>High</option>
                  <option>Medium</option>
                  <option>Low</option>
                </select>
              </div>

              <div className = "deadline-container"> 
                <label className = "deadline-label">Deadline</label>
                <input 
                  className = "deadline-input" 
                  type = "date" 
                  placeholder="Deadline" 
                  ref = {this.deadline} 
                  value = {this.state.modifyingDeadline 
                    ? this.state.deadlineShowing 
                    : `${this.state.deadlineShowing.split('.')[2]}-${this.state.deadlineShowing.split('.')[1]}-${this.state.deadlineShowing.split('.')[0]}`}
                  onChange = {this.handleDeadlineChange}
                />
              </div>

              <div className = "div-button-create" onClick = {this.handleModifyButton}>Modify the task</div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
