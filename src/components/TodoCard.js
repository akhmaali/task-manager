import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent'
import { Draggable } from 'react-beautiful-dnd';
import './styles/ToDoCard.css';

/**
 * @component TodoCard - represents individual card in a board
 */
class TodoCard extends React.Component {

    /**
     * Constructor for TodoCard component
     * State contains card id received from props which has to be transformed on drag start event
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            cardIdToTransform: this.props.id
        }
        this.triggerDeleteButton = this.triggerDeleteButton.bind(this);

        this.svgRef = React.createRef();
    }

    /**
     * Deletes the board after firing delete event on board
     */
    triggerDeleteButton = (e) => {
        e.stopPropagation();
        let boards =  JSON.parse(localStorage.getItem('main'));
        let searchedBoard = boards.find(board => board.cards.find(card => card.id === this.props.id));
        let card = searchedBoard.cards.find(card => card.id === this.props.id);
        let position = searchedBoard.cards.indexOf(card);
        searchedBoard.cards.splice(position, 1);
        localStorage.setItem('main', JSON.stringify(boards));
        this.props.updateAppComponent();
    }

    /**
     * Changes the color of svg path of delete icon to darkred on mouse over event 
     */
    handleMouseOver = (e) => {
        this.svgRef.current.style.fill = "darkred";
    }

    /**
     * Changes the color of svg path of delete icon to black on mouse leave event
     */
    handleMouseLeave = (e) => {
        this.svgRef.current.style.fill = "black";
    }

    /**
     * Renders TodoCard component
     * Each card is Draggable
     */
    render() {
        return(
            <Draggable draggableId = {String(this.props.id)} index = {this.props.index}>
                {provided => (
                    <div 
                        className = "card-container"
                        ref = {provided.innerRef} 
                        {...provided.draggableProps} 
                        {...provided.dragHandleProps}
                    > 
                        <Card style = {{transform: this.props.cardIdToTransform === this.props.id ? 'rotateZ(8deg) rotateX(15deg)' : ''}}>
                            <CardContent className = "card">
                                <h4 className = "cardName">
                                    {this.props.title}
                                </h4>
                            <svg className="svg-icon svg-delete" viewBox="0 0 20 20" onClick = {this.triggerDeleteButton} onMouseOver = {this.handleMouseOver} onMouseLeave = {this.handleMouseLeave}>
                                <path ref = {this.svgRef} d="M17.114,3.923h-4.589V2.427c0-0.252-0.207-0.459-0.46-0.459H7.935c-0.252,0-0.459,0.207-0.459,0.459v1.496h-4.59c-0.252,0-0.459,0.205-0.459,0.459c0,0.252,0.207,0.459,0.459,0.459h1.51v12.732c0,0.252,0.207,0.459,0.459,0.459h10.29c0.254,0,0.459-0.207,0.459-0.459V4.841h1.511c0.252,0,0.459-0.207,0.459-0.459C17.573,4.127,17.366,3.923,17.114,3.923M8.394,2.886h3.214v0.918H8.394V2.886z M14.686,17.114H5.314V4.841h9.372V17.114z M12.525,7.306v7.344c0,0.252-0.207,0.459-0.46,0.459s-0.458-0.207-0.458-0.459V7.306c0-0.254,0.205-0.459,0.458-0.459S12.525,7.051,12.525,7.306M8.394,7.306v7.344c0,0.252-0.207,0.459-0.459,0.459s-0.459-0.207-0.459-0.459V7.306c0-0.254,0.207-0.459,0.459-0.459S8.394,7.051,8.394,7.306"></path>
                            </svg>
                            </CardContent>
                        </Card>
                    </div>
                )}
            </Draggable>
        );
    }
}

export default TodoCard;