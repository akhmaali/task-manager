import React from 'react';
import Icon from '@material-ui/core/Icon';
import Card from '@material-ui/core/Card';
import Textarea  from 'react-textarea-autosize';
import Button from '@material-ui/core/Button';
import {v4 as uuidv4} from 'uuid';
import './styles/ActionButton.css';

/**
 * @component ActionButton - represents add card/add board 
 */
class ActionButton extends React.Component {

    /**
     * Constructor for ActionButton component
     * State contains boards, which are received from localstorage on componentDidMount
     * formOpen - indicates whether to open the form or close
     * @param {*} props 
     */
    constructor(props) {
        super(props);
        this.state = {
            boards: [],
            formOpen: false,
            text: ''
        }
    }

    /**
     * Updates the state by setting openForm to true and text to empty string
     */
    openForm = () => {
        this.setState({
            formOpen: true,
            text: ""
        })
    }

    /**
     * Updates the state by setting fromOpen to false. Closes the form
     */
    closeForm = (e) => {
        this.setState({
            formOpen: false
        })
    }

    /**
     * Controls the input change
     */
    handleInputChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }

    /**
     * Triggers add board button
     * Boards are retrived from localStorage
     * New board is pushed and changes are persisted back to localStorage
     * App component is updated by Component Drilling
     */
    handleAddBoard = () => {
        if(this.state.text.length !== 0) {
            const newBoard = {
                title: this.state.text,
                id: `board-${uuidv4()}`,
                cards: []
            };
    
            let boards = JSON.parse(localStorage.getItem('main'));
            boards.push(newBoard);
            localStorage.setItem('main', JSON.stringify(boards));
            this.props.updateAppState();
        }
    }

    /**
     * Triggers add card button
     * Sets the date of the creating card to today's and formats the date
     * Generated card id by uuidv4
     * Sets card priority to High by default
     * Gets board and card array for this board from localStorage
     * New card is pushed to card array for the corresponing board
     * Changes are persisted back to localStorage
     */
    handleAddCard = () => {
        if(this.state.text.length !== 0) {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
            var yyyy = today.getFullYear();
            today = `${dd}.${mm}.${yyyy}`;
    
            const newCard = {
                id: `card-${uuidv4()}`,
                date: today,
                title: this.state.text,
                description: '',
                priority: 'High',
                deadline: ''
            }
    
            //Find the board and add card to it -> set state
            let tempState = JSON.parse(localStorage.getItem('main'));
            const searchedBoard = tempState.find( board => board.id === this.props.boardID);
            searchedBoard.cards.push(newCard);
            localStorage.setItem('main', JSON.stringify(tempState));
            //update App component
            this.props.updateAppComponent();
        }
    }

    /**
     * Sets the boards property in state to boards received from localStorage
     */
    componentDidMount() {
        let newBoards = JSON.parse(localStorage.getItem('main'));
        this.setState({
            boards: newBoards
        });
    }


    /**
     * Renders a form to fill title for new board/card depending on the props received
     */
    renderForm = () => {
        const { board } = this.props;
        const placeholder = board ? "Enter board title..." : "Enter card title...";
        const buttonTitle = board ? "Add board" : "Add card";

        return (
            <div>
                <Card className = "card-style">
                    <Textarea 
                        placeholder = {placeholder}
                        autoFocus
                        onBlur = { this.closeForm }
                        value = { this.state.text }
                        onChange = { this.handleInputChange }
                        className = "textarea-style"
                    />
                </Card>
                <div className = "formButtonGroup">
                    <Button 
                        onMouseDown = {board ? this.handleAddBoard : this.handleAddCard}
                        variant = "contained" 
                        style = {{color: "white", backgroundColor: "#5aac44"}}
                    >
                        {buttonTitle}{" "}
                    </Button>
                    <div onClick = {() => this.setState({formOpen: false})} className = "close-card-or-board">
                        <Icon>close</Icon>
                    </div>
                </div>
            </div>
        )
    }

    /**
     * Renders add button for board/card depending on the props received
     */
    renderAddButton = () => {
        const { board } = this.props;
        const buttonText = board ? "Add another board" : "Add another card";
        return (
            <div 
                onClick = {this.openForm}
                className = "openFormButtonGroup"
            >
                <Icon>add</Icon>
                <p>{buttonText}</p>
            </div>
        )
    }

    /**
     * Renders ActionButton component
     */
    render() {
        return this.state.formOpen ? this.renderForm() : this.renderAddButton() ;
    }

}

export default ActionButton;
